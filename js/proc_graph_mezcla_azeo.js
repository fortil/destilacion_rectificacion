var pixs = 400;
var platos;
var registro = {};
var Xd;
var deltaY, deltaX;

jQuery(document).ready(function($) {
	drawInitGraph();
	$('#draw').click(function(event) {
		event.preventDefault();
	});
	$('#btnCalcular').click(function(event) {
		//limpiar el lienzo
		clearCanvas();
		//inicia las lineas principales
		drawInitGraph();
		//dibuja linea de equilibrio
		drawFunctionCurvaEq();
		var Xf = relacionMolarF();
		var Xd = relacionMolarD();
		var Xw = relacionMolarW();
		//dibuja las rectas verticales
		drawFunctionX(Xf,'#FF0000');
		drawFunctionX(Xd,'#5FB404');
		drawFunctionX(Xw,'#00FFFF');
		//dibuja los platos teóricos
		drawQ(Xf,Xd,Xw);
		eficienciaMasica(Xf,Xd,Xw);
		HallarYn(Xd);
		drawPlatosTeoricos(Xw,true,Xd,Xf);
	});
	$('#file').on('change', function(event) { 
		var file = $('#file')[0].files[0];
		var reader = new FileReader();
		reader.onload = readText;
		reader.readAsText(file);
	})

});
//iniciar gráfica principal
function drawInitGraph() {
	drawFunctionTendencia() 
	drawAxis()
}

//función calcular relación molar de W
function relacionMolarW() {
	var Xw = +$('#inputXw').val();
	var W = +$('#inputF').val() - $('#outputD').html();
	//columna 2
	$('#outputWmenosXw').html((1-Xw).toFixed(3));
	//columna 1
	$('#outputXw').html((W*Xw).toFixed(3));
	$('#outputWmenosPorw').html((W*(1-Xw)).toFixed(3));
	$('#outputW').html((W).toFixed(3));
	return Xw;
}
//función calcular relación molar de D
function relacionMolarD() {
	Xd = +$('#inputXd').val();
	var Xw = +$('#inputXw').val();
	var _D = +$('#outputFxf').html() - (+$('#inputF').val()*Xw);
	var D_ = Xd - Xw;
	var D = _D / D_;
	//columna 2
	$('#outputDmenosXd').html((1-Xd).toFixed(3));
	$('#outputTotalD').html((Xd  + 1-Xd).toFixed(3));
	//columna 1
	$('#outputDxd').html((D*Xd).toFixed(3));
	$('#outputDmenosPord').html((D*(1-Xd)).toFixed(3));
	$('#outputD').html((D).toFixed(3));
	return Xd;
}
//función calcular relación molar de F
function relacionMolarF() {
	var xf = +$('#inputXf').val();
	var F = +$('#inputF').val();
	//columna 1
	$('#outputFxf').html((xf*F).toFixed(3));
	$('#outputFmenosPorf').html(((1-xf)*F).toFixed(3));
	$('#outputF').html((F).toFixed(3));
	//columna 2
	$('#outputFmenosXf').html((1-xf).toFixed(3));
	$('#outputTotal').html((+$('#outputFmenosXf').html()+xf).toFixed(3));
	return xf;
}
//funcion transformar `puntos a pixeles
function pTP(punto) {
	// Math.abs(( 0.050 * 400 ) - 400)
	return Math.abs(( punto * 400 ) - 400)
}
//dibujar ejes
function drawAxis() {
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = '#000000';
	//line X
	ctx.moveTo(0,400);
	ctx.lineTo(400,400);
	//line Y
	ctx.moveTo(0,0);
	ctx.lineTo(0,400);
	ctx.stroke();
}

//dibujar la funcíón X,Y
function drawFunctionCurvaEq() {
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	var valores = registro.data;
	ctx.strokeStyle = '#0000ff';
	for (var i = 1, lenX = valores.X1.length; i < lenX; i++) {
		ctx.moveTo(valores.X1[i-1]*pixs,pTP(valores.Y1[i-1]));
		ctx.lineTo(valores.X1[i]*pixs,pTP(valores.Y1[i]));
		ctx.stroke();
	};
}
//dibuja grafica de tendencia
function drawFunctionTendencia() {
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = '#C2C2C2';
	for (var i = 0; i < 1; i++) {
		ctx.moveTo(i*pixs,pTP(i));
		ctx.lineTo((i+1)*pixs,pTP(i+1));
		ctx.stroke();
	};
}
//dibuja gráfica X
function drawFunctionX(X,color) {
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = color;
	for (var i = 0; i <= X; i = i + 0.01) {
		ctx.moveTo(X*pixs,pTP(i));
		ctx.lineTo(X*pixs,pTP(i+0.01));
		ctx.stroke();
	};
}
function Intersec() {
	var valores = registro.data;
	var valorMax = {};
	for (var i = 0; i < valores.Y1.length; i++) {
		for (var e = 0; e < valores.X1.length; e++) {
			// X = 0.54, Y = 0.54
			if( (valores.X1[e]-0.001)<valores.Y1[i] && valores.Y1[i]<(valores.X1[e]+0.001) && e!=0){
				valorMax = {'x':valores.X1[i],'y':valores.Y1[i]};
				return valorMax;
			}
		};
	};
	return valorMax;
}

//interpolar Y
function interpolarY(i,Xi) {
	var val = registro.data;
	var  Y = val.Y1[i-1] + (Xi-val.X1[i-1])*((val.Y1[i]-val.Y1[i-1]) / (val.X1[i]-val.X1[i-1]))
	return Y;
}
//encontrar punto en los puntos de la linea equilibrio
function findPointEq(Y) {
	var valores = registro.data;
	var valorMax = 0;
	for (var i = Y; i < 1; i = i + 0.0001) {
		for (var e = 0; e < valores.X1.length; e++) {
			if ( i<valores.X1[e]) {
				valorMax = interpolarY(e,i);
				return valorMax;
			};
		};
	};
}
//encontrar punto X de recta 2
function findPointX(argument) {
	
}
//dibujar platos teóricos
function drawPlatosTeoricos(xw,verdadero,XD,Xf) {
	var z = 0
	var intersec = new Intersec();
	var Ymin = xw;
	var Ymax;
	var X;
	while(verdadero){
		var c=document.getElementById("GraphCanvas");
		var ctx=c.getContext("2d");
		ctx.beginPath();
		ctx.strokeStyle = '#00ff00';

		Ymax = findPointEq(xw);

		ctx.moveTo(xw*pixs,pTP(Ymin));
		ctx.lineTo(xw*pixs,pTP(Ymax));
		ctx.stroke();

		X = Math.abs(((Ymax-xw)/(deltaY-xw))*(deltaX-xw) + xw);

		ctx.moveTo(xw*pixs,pTP(Ymax));
		ctx.lineTo(X*pixs,pTP(Ymax));
		ctx.stroke();

		if (Math.abs(xw-deltaX)<0.04 || xw>=deltaX) {
			platos = z;
			verdadero = false;
			break;
		}else{
			xw = X;
			Ymin = Ymax;
			z ++;
		};
		/*if (Math.abs(intersec.x-xw)<=0.14 || Math.abs(intersec.y-Ymax)<=0.04) {
			verdadero = false;
		}else{
		}*/
	}
	platosXDmayor(X,Ymax,XD,true)
}
//dibujar platos si XD es mayor
function platosXDmayor(Xo,Yo,XD,verdadero) {
	var z = 0;
	var Xa = Xo;
	while(verdadero){
		var c=document.getElementById("GraphCanvas");
		var ctx=c.getContext("2d");
		ctx.beginPath();
		ctx.strokeStyle = '#00ff00';

		var X = Math.abs(((Yo-deltaY)/(XD-deltaY))*(XD-deltaX) + deltaX);
		if (Xo == Xa) {
			ctx.moveTo(Xo*pixs,pTP(Yo));
			ctx.lineTo(X*pixs,pTP(Yo));
			ctx.stroke();
		}else{
			var dif = (Math.abs(X-Xo)>0.10)? 0.1: 0.01;
			console.log(Math.abs(X-Xo))
			for (var i = Xo; Math.abs(X-i)>dif; i = i + 0.01) {
				ctx.moveTo(i*pixs,pTP(Yo));
				ctx.lineTo((i+0.01)*pixs,pTP(Yo));
				ctx.stroke();
			};
		};

		if (Math.abs(X-XD)<0.04 || X>=XD) {
			verdadero = false;
			z ++;
			break;
		};

		var Ymax = findPointEq(X);

		ctx.moveTo(X*pixs,pTP(Yo));
		ctx.lineTo(X*pixs,pTP(Ymax));
		ctx.stroke();

		Xo = X;
		Yo = Ymax;
		z ++;
	}
	platos = platos + z ;
	$('#PlatosTeoricos').html(platos)
}
//reiniciar canvas
function clearCanvas() {
	var c = document.getElementById("GraphCanvas");
	var ctx = c.getContext("2d");
	ctx.clearRect(0, 0, c.width, c.height);
}

//funcion para leer archivos de data
function readText(progressEvent) {
  var lines = this.result.split('\n');
  var llength = lines.length;

  var presion = (lines[0].split('=')[1]).trim();
  var X = [];
  var Y = [];
  registro = {};

  for (var i = 1; i < llength; i++) {
  	var dt = lines[i].split(',');
  	if ($.isNumeric(+dt[0]) && $.isNumeric(+dt[1])) {
	  	X.push(+dt[0])
	  	Y.push(+dt[1])
  	};
  };
  registro.presion = presion;
  registro.data = {'X1':X,'Y1':Y};
  fillTable();
  drawFunctionCurvaEq();
  $('#titleData').empty().html('Datos a '+registro.presion)
  $('#btnCalcular').removeAttr('disabled')
}
//funcion que llena la tabla de valores
function fillTable () {
	var sel = $('table.data tbody');
	sel.empty();
	for (var i = 0, l = registro.data.X1.length; i < l; i++) {
		sel.append('<tr><td>'+registro.data.X1[i]+'</td><td>'+registro.data.Y1[i]+'</td>');
	};
}
//funcion que dibuja Q
function drawQ(Xf,Xd,Xw) {
	var q = +$('#inputQ').val();
	//X inicial Xf, Y inicial Xf
	var atan = Math.atan(q);
	var Xfl = Xf*((q==0)?0.5:q);
	var Yf = findPointEq(Xfl);
	
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = '#DF7401';
	ctx.moveTo(Xf*pixs,pTP(Xf));
	ctx.lineTo(Xfl*pixs,pTP(Yf));
	ctx.stroke();
	deltaY = (Yf-Xf)/2 +Xf;
	deltaX = (Xfl-Xf)/2 +Xf;
	lineaUnoCruzada(deltaX,deltaY,Xd,Xf,Yf);
	lineaDosCruzada(deltaX,deltaY,Xw,Xf,Yf)
}
//hallar la eficiencia
function eficienciaMasica(Xf,Xd,Xw) {
	var F = +$('#inputF').val();
	var Ff = F*65.4;
	var _D = (Ff*Xf) - (Ff*Xw);
	var D_ = Xd - Xw;
	var D = _D / D_;
	var Eficiencia = (D * Xd)/(Ff * Xf)*100;
	$('#EficienciaMasica').empty().html(Eficiencia.toFixed(1)+"%");
}
//funcion hallar Yn+1
function HallarYn(Xd) {
	var R = +$('#inputR').val();
	var Xn = +$('#inputXn').val();
	var Yn = (R/(R+1)*Xn + Xd/(R+1));
	$('#Yn').empty().html(Yn.toFixed(2));
}
//funcion que cruza la linea Uno
function lineaUnoCruzada(dX,dY,Xd,Xf,Yf) {
	//Y-Y1 = m(X-X1)
	dX = (dX==0)?Xf:dX;
	dY = (dY==0)?Yf:dY;
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = '#f0ff00';
	ctx.moveTo(Xd*pixs,pTP(Xd));
	ctx.lineTo(dX*pixs,pTP(dY));
	ctx.stroke();
}
//funcion que cruza la linea Dos
function lineaDosCruzada(dX,dY,Xw,Xf,Yf) {
	//Y-Y1 = m(X-X1)
	dX = (dX==0)?Xf:dX;
	dY = (dY==0)?Yf:dY;
	var c=document.getElementById("GraphCanvas");
	var ctx=c.getContext("2d");
	ctx.beginPath();
	ctx.strokeStyle = '#f0ff00';
	ctx.moveTo(Xw*pixs,pTP(Xw));
	ctx.lineTo(dX*pixs,pTP(dY));
	ctx.stroke();
}


function interseccion() {
	// (x-Xd)/(Ymax-Xd) = (Y-Ymax)/(Ymax-Ymax)
}